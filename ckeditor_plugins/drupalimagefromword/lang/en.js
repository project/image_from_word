CKEDITOR.plugins.setLang('drupalimagefromword', 'en', {
    dialog_title_error: 'Error | Image from Word',
    error_text: 'Some images cannot be pasted together with other content due the security reasons.',
    error_recommendation: "Please, check that your MS Word document is not in restricted editing mode." +
    "Also, you can try to paste images separately from text." +
    "If it doesn't help, then you should upload images as separate files."
});
